# Sciam Tech Review - Quarkus getting started

> Date: 18 Décembre 2020
>
> Auteur: Pierre Cesmat <pierre.cesmat@sciam.fr>
>
> URL de la vidéo: [#4 Quarkus : Supersonic Subatomic Java](https://caitsconsulting.sharepoint.com/:v:/s/SCIAMCommunicationclients/EXe9Qmn8MiNAq16wdZyIbTMBezQt5fcNQjyKdSWefhesKg?e=wS2uua)

L'émergence du Cloud ces dernières années a permis à l'architecture microservice de se développer et de devenir, aujourd'hui une des plus performante et utilisée dans le développement d'applications.

Malheureusement, le Java a bien du mal à suivre cette évolution et répond difficilement aux exigences de cette architecture.

Heureusement pour vous, Quarkus, un framework Java basé sur les librairies les plus utilisées dans la création de microservice, a peut être une solution.

Je vous propose d'explorer dans ce guide la facilité d'utilisation de ce framework ainsi que les optimisations que celui-ci peut effectuer sur votre application.

Vous pouvez retrouver la présentation au format pdf en suivant ce lien [Quarkus-getting-started](Quarkus-getting-started.pdf)

**PS :Tous les scripts de ce tutoriel ont été créé afin de fonctionner avec powershell, une invite de commande Windows**

### Prérequis

* Java JDK8+ installé sur votre poste et la variable d'environement JAVA_HOME existante
  * https://tutorials.visualstudio.com/Java/hello-world/install-jdk pour plus d'information
* Maven 3.6.3+ installé sur votre poste 
  * https://tutorials.visualstudio.com/Java/hello-world/install-maven pour plus d'information
* Docker 19.03.8+ installé sur votre poste
  * https://docs.docker.com/get-docker/ pour plus d'information
* Intellij IDEA installé sur votre poste
  * https://www.jetbrains.com/fr-fr/idea/download/ pour le télécharger, ensuite suivez les instructions

Si vous souhaitez déployer sur le Cloud, ces prérequis seront nécessaires :
* Azure CLI 2.16.0+ doit être installé
  * https://docs.microsoft.com/fr-fr/cli/azure/install-azure-cli pour plus d'information
* Vous avez besoin d'un compte azure si vous souhaitez vous essayer au déploiement sur le cloud
  * https://azure.microsoft.com/fr-fr/free/ pour créer un compte
  
### Exercices

1. ##### [quarkus-quickstart](quarkus-quickstart.md)

Cet exercice vous permettra de prendre en main le framework et de découvrir comment il est simple et rapide de créer un microservice

2. ##### [todo-backend-quarkus-docker](todo-backend-quarkus-docker/README.md)

Cet exercice vous permettra de découvrir la conteneurisation avec quarkus + docker ainsi que la création d'image native encore une fois, de manière simple grâce à Quarkus

3. ##### [todo-backend-quarkus-azure](todo-backend-quarkus-azure/README.md)

Cet exercice vous permettra de découvrir le déploiement de conteneurs sur le Cloud, et plus précisément sur Azure

### Pour commencer

* Ouvrez une invite de commande
* Dirigez-vous dans votre répertoire de travail \
`cd C:/mon/espace/de/travail`
* Clonez le projet \
`git clone https://gitlab.com/sciam/tech-review/quarkus-getting-started.git`
* Entrez dans le dossier qui vient d'être créé \
`cd quarkus-getting-started`
* Vous pouvez maintenant commencer les exercices
