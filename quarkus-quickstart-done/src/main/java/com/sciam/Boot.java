package com.sciam;

import io.quarkus.runtime.StartupEvent;

import javax.enterprise.event.Observes;
import javax.transaction.Transactional;

public class Boot {

    @Transactional
    void init(@Observes StartupEvent event) {
        Personne.builder()
                .firstName("John")
                .name("Doe")
                .build().persistAndFlush();
        Personne.builder()
                .firstName("Johnny")
                .name("Bigoud")
                .build().persistAndFlush();
    }
}
