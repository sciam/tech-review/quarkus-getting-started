package com.sciam;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/personne")
public class PersonneResource {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listAll() {
        return Response.accepted(Personne.listAll()).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response getOne(@PathParam("id") Long id) {
        return Response.accepted(Personne.findById(id)).build();
    }
}