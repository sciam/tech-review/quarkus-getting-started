# quarkus-quickstart

Le but de cet exercice est de vous faire découvrir le framework Quarkus et les outils/optimisations qu'il met à disposition des développeurs pour leur rendre plus facile, le développement de microservices.

### Exercice

Pour commencer, nous allons créer un projet à l'aide du plugin maven Quarkus
1. Pour se faire, exécutez le fichier create-project.ps1\
`./create-project.ps1`
1. Déplacez-vous dans le projet créé\
`cd quarkus-quickstart`
1. Ouvrez le projet dans IntelliJ\
"File" > "Open" et sélectionnez le projet quarkus-quickstart à l'emplacement `C:/mon/espace/de/travail/tech-review-quarkus/quarkus-quickstart`
1. Si le projet n'apparait pas correctement, faites un clique droit sur le fichier "pom.xml" > "maven" > "reload project"

1. Ajoutez la dépendance Lombok dans votre pom.xml, entre les balises <dependencies\>
    ```
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <version>1.18.12</version>
    </dependency>
    ```
1. Lancez votre application en utilisant la commande `mvn quarkus:dev` dans votre invite de commande
1. Naviguez sur la page http://localhost:8080 \
La page d'accueil de votre application devrait apparaître
1. Naviguez sur la page http://localhost:8080/hello-resteasy \
Vous devriez voir apparaître Hello RESTEasy
1. Créez une classe Personne \
Clique droit sur quarkus-quickstart/src/main/java/com.sciam > "New" > "Java Class" > "Personne"
    ```
    package com.sciam;
    
    import io.quarkus.hibernate.orm.panache.PanacheEntity;
    import lombok.*;
    
    import javax.persistence.Entity;
    
    @Entity
    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public class Personne extends PanacheEntity {
        String name;
        String firstName;
    }
    ```
1. Créez la resource associée PersonneResource \
Clique droit sur quarkus-quickstart/src/main/java/com.sciam > "New" > "Java Class" > "PersonneResource"
    ```
    package com.sciam;
    
    import javax.ws.rs.GET;
    import javax.ws.rs.Path;
    import javax.ws.rs.PathParam;
    import javax.ws.rs.Produces;
    import javax.ws.rs.core.MediaType;
    import javax.ws.rs.core.Response;
    
    @Path("/api/personne")
    public class PersonneResource {
        @GET
        @Produces(MediaType.APPLICATION_JSON)
        public Response listAll() {
            return Response.accepted(Personne.listAll()).build();
        }
    
        @GET
        @Produces(MediaType.APPLICATION_JSON)
        @Path("/{id}")
        public Response getOne(@PathParam("id") Long id) {
            return Response.accepted(Personne.findById(id)).build();
        }
    }
    ```
1. Créez la classe Boot qui va nous permettre d'ajouter des personnes en base de donnée\
Clique droit sur quarkus-quickstart/src/main/java/com.sciam > "New" > "Java Class" > "Boot"
    ```
    package com.sciam;
    
    import io.quarkus.runtime.StartupEvent;
    
    import javax.enterprise.event.Observes;
    import javax.transaction.Transactional;
    
    @Transactional
    void init(@Observes StartupEvent event) {
        Personne.builder()
                .firstName("John")
                .name("Doe")
                .build().persistAndFlush();
        Personne.builder()
                .firstName("Johnny")
                .name("Bigoud")
                .build().persistAndFlush();
    }
    ```
1. Ajoutez dans votre fichier application.properties qui se trouve dans quarkus-quickstart/src/main/resources
    ```
   # Hibernate
   quarkus.datasource.jdbc.url=jdbc:postgresql://localhost:5432/rest-crud
   quarkus.datasource.db-kind=postgresql
   quarkus.datasource.username=restcrud
   quarkus.datasource.password=restcrud
   quarkus.hibernate-orm.database.generation=drop-and-create
   quarkus.hibernate-orm.log.sql=true
   ```