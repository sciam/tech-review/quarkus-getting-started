# todo-backend-quarkus-azure

Le but de cet exercice est de vous faire découvrir le déploiement de conteneurs sur le Cloud Azure.

## Azure

Création de notre ressource Azure ainsi que des différents composants via une invite de commande

1. Assurez-vous que votre invite de commande soit placée à la racine du projet "todo-backend-quarkus-azure"\
Si ce n'est pas le cas, exécutez la ligne de commande suivante : \
`C:/mon/espace/de/travail/quarkus-quickstart/todo-backend-quarkus-azure`
1. Connexion à Azure \
`az login`
1. Création d'un groupe de ressource\
`az group create -l eastus --name tech-review-quarkus`
1. Création d'un serveur pour notre base de donnée\
`az postgres server create --name tech-review-quarkus-server --resource-group tech-review-quarkus --public all --admin-user famousCoconut3 --admin-password ZqFGkNKw5BTMJhevhJCH0w`
1. Création de la base de donnée postgres\
`az postgres db create --resource-group tech-review-quarkus --server tech-review-quarkus-server --name tech-review-quarkus-db`
1. Création d'un repository de conteneur\
`az acr create --resource-group tech-review-quarkus --name pedroelmoucho --sku Basic --admin-enabled true`
1. Authentification de docker auprès du repo\
`az acr login --name pedroelmoucho`
1. Récupération de l'url de login du repository que nous appellerons <acr_repo_url> par la suite\
`az acr show --name pedroelmoucho --query loginServer`
1. Récupération ddu login et du password du repository que nous appellerons <acr_repo_login> et <acr_repo_password> par la suite\
`az acr credential show --name pedroelmoucho`

Maintenant que notre environnement Azure est prêt, nous allons générer des images docker de notre application pour pourvoir les déployer sur le cloud

1. Exécutez la ligne de commande suivante pour générer une image docker de notre application qui va être exécutée par une JVM\
`./script/build-docker-jvm.ps1`
1. Exécutez cette autre ligne de commande suivante pour générer une image docker de notre application qui va être exécutée de façon native\
`./script/build-docker-native.ps1`
1. Une fois que nos images ont été générées, nous devons les publier sur le repository Azure que nous avons créé
    ```
    docker tag pedroelmoucho/todo-backend-azure-jvm:1.0-SNAPSHOT <acr_repo_url>/pedroelmoucho/todo-backend-azure-jvm
    docker tag pedroelmoucho/todo-backend-azure-native:1.0-SNAPSHOT <acr_repo_url>/pedroelmoucho/todo-backend-azure-native
    docker push <acr_repo_url>/pedroelmoucho/todo-backend-azure-jvm
    docker push <acr_repo_url>/pedroelmoucho/todo-backend-azure-native
   ```
1. Enfin, nous devons déployer les conteneurs pour que nos applications se lancent et soient disponibles\
Chaque ligne de commande vous retournera l'url de votre conteneur que vous pourrez utiliser dans votre navigateur pour le joindre
   ```
    az container create --name tech-review-quarkus-jvm --resource-group tech-review-quarkus `
    --image <acr_repo_url>/pedroelmoucho/todo-backend-azure-jvm `
    --registry-login-server pedroelmoucho.azurecr.io `
    --registry-username <acr_repo_login> `
    --registry-password <acr_repo_password> `
    --dns-name-label tech-review-quarkus-jvm-1542862 `
    --query ipAddress.fqdn
    
    az container create --name tech-review-quarkus-jvm --resource-group tech-review-quarkus `
    --image <acr_repo_url>/pedroelmoucho/todo-backend-azure-native `
    --registry-login-server pedroelmoucho.azurecr.io `
    --registry-username <acr_repo_login> `
    --registry-password <acr_repo_password> `
    --dns-name-label tech-review-quarkus-jvm-1542365 `
    --query ipAddress.fqdn
   ```
1. Si tout s'est bien passé, lorsque vous allez essayer de vous connecter à l'url de votre conteneur dans votre navigateur, vous devriez tomber sur cette page
![](img/home-page.png (200x200))