# todo-backend-quarkus-docker

Le but de cet exercice est de vous faire découvrir la conteneurisation de votre application avec Quarkus pour Docker

Tout d'abord, nous allons commencer par créer nos images.

1. Assurez-vous que votre invite de commande soit placée à la racine du projet "todo-backend-quarkus-docker"\
Si ce n'est pas le cas, exécutez la ligne de commande suivante : \
`C:/mon/espace/de/travail/quarkus-quickstart/todo-backend-quarkus-docker`
1. Exécutez la ligne de commande suivante pour générer une image docker de notre application qui va être exécutée par une JVM\
`./script/build-docker-jvm.ps1`
1. Exécutez cette autre ligne de commande suivante pour générer une image docker de notre application qui va être exécutée de façon native\
`./script/build-docker-native.ps1`
1. Ensuite, nous allons devoir exécuter une base de donnée pour que nos applications puissent s'y connecter\
`./script/start-database.ps1`
1. Enfin, vous pouvez lancer les deux conteneurs sur votre docker local afin d'observer les différences
   ```
    ./script/run-docker-jvm.ps1
    ./script/run-docker-native.ps1
   ```
1. Si tout s'est bien passé, vous deviez voir apparaitre vos 3 conteneurs si vous exécutez un `docker ps`\

![](img/docker-ps.png (400x200))